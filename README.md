# Deep Learning-Based Localization of EEG Electrodes Within MRI Acquisitions

Code associated with the article : "Deep Learning-Based Localization of EEG Electrodes Within MRI Acquisitions" (https://doi.org/10.3389/fneur.2021.644278).